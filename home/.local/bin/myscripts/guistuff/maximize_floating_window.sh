#!/bin/sh

# This script maximizes a floating window. Maximizing is different from fullscreening because some windows behave differently when fullscreened.
# Dependencies: i3-msg and jq. Inspiration: https://gist.github.com/michaelmrose/a429a0fc945e7eab3df06f4cf667492a - Thanks michaelrose!

# Check whether jq is present on the system. If not, then throw a warning using the i3 nagbar.
if [ -f "$(command -v jq)" ]; then
	i3_workspaces_json="$(i3-msg -t get_workspaces)"
	available_dimensions="$(echo "$i3_workspaces_json" | jq -r ".[] | select(.focused == true)|{w: .rect.width, h: .rect.height}| .[]" | tr '\n' ' ')"
	focused_display="$(echo "$i3_workspaces_json" | jq .[]|jq -r "select(.focused == true).output")"

	# Resize to Available Dimensions (this makes it so that things like bars can stay visible)
	i3-msg "resize set "$available_dimensions" px, move position 0 0"

	# This stuff is to make Chromium less of a hassle when in maximized floating mode.
	# This way I can at least click the Chromium browser tabs when my mouse cursor is on top edge of the screen etc.
	# However, for some reason I have to put this here because it gets ignored in the i3 config.
	i3-msg '[con_id=__focused__ class="(?i)chromium"] move position -5 -10 px, resize grow down 15 px, resize grow right 10 px'
	# Also: Syntax for move is: move position <pos_x> [px|ppt] <pos_y> [px|ppt] (https://i3wm.org/docs/userguide.html#move_direction)
	# Sometimes adjusting the <pos_y> is necessary because the Chromium window sometimes tends to go higher? Kind of weird.
else
	i3-nagbar --message "jq not found! This is required for ~/.local/bin/myscripts/guistuff/maximize_floating_window.sh to work!"
fi
