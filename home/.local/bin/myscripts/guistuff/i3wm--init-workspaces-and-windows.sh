#!/usr/bin/env sh

# Slightly taken from: https://unix.stackexchange.com/a/97081

f__pause () { sleep 0.4 ;}

i3-msg 'workspace 1; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/01_ubuntumono.yml --title T.admin'
f__pause
i3-msg 'workspace 2; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/03_gohufont.yml --title term-chromium'
f__pause
i3-msg 'workspace 3; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/03_gohufont.yml --title term-firefox'
f__pause
i3-msg 'workspace 4; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/03_gohufont.yml --title T.logstats'
f__pause
i3-msg 'workspace F1; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/03_gohufont.yml --title al-F1'
f__pause
i3-msg 'workspace F2; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/03_gohufont.yml --title al-F2'
f__pause
i3-msg 'workspace F3; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/01_ubuntumono.yml --title T.remote'
f__pause


# The most "complicated" workspace: Media.
i3-msg 'workspace 6; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/02_terminus.yml --title T.media'
i3-msg 'workspace 6; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/03_gohufont.yml --title alsamixer --command alsamixer --view all'
i3-msg 'workspace 6; exec alacritty__wrapper.sh --config-file ~/.config/alacritty/03_gohufont.yml --command pulsemixer --no-mouse'

#[ "$(command -v layout_manager > /dev/null)" ] && layout_manager ws-media
#[ -f ~/sources/i3-layout-manager/layout_manager.sh ] && ~/sources/i3-layout-manager/layout_manager.sh ws-media


# Not really i3wm related, but otherwise some Flatpak apps are going to complain about their file picker (has something to do with xdg-desktop-portal.service)
command -v systemctl > /dev/null && systemctl --user import-environment DISPLAY
