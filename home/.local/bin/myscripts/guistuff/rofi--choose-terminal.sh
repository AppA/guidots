#!/bin/sh

# This is a script that gets called when I want to select a terminal. Alacritty for example can take a custom config as argument to the --config-file flag.
# In this script, I add various terminal (configurations) to a list, which gets put into a variable (called... list)
# Then I pass that variable to a rofi dmenu, when I make a selection in the rofi dmenu, it gets run by the shell.

# Define the Alacritty config directory
alacritty_config_dir="$HOME/.config/alacritty"

# Automatically find Alacritty config files
alacritty_configs=$(echo "$alacritty_config_dir"/* | sed 's| |\n|g' | while read -r config ; do
	config_name="${config##*/}"
	relative_path="~/.config/alacritty/$config_name"
	echo "alacritty__wrapper.sh --config-file $relative_path"
done)

# Create the list of terminal options
list=$(printf "%s\n" "$alacritty_configs" "st" "uxterm")

# Call rofi menu with fuzzy search and make the search case-insensitive
selected=$(echo "$list" | rofi -dmenu -i -p "Choose Your Terminal" -matching fuzzy)

[ -z "$selected" ] && exit

# Run the selected terminal
sh -c "$selected"
