#!/usr/bin/env sh

# i3wm Window Tree Viewer
# This script displays a hierarchical view of i3wm workspaces, containers and windows.
# Partially generated by Claude 3.5 Sonnet

# Usage:
#   i3-msg -t get_tree | ./i3wm_tree_viewer.sh
#   or
#   ./i3wm_tree_viewer.sh input_file.json

# Check for missing dependencies.
for dep in \
	awk \
	jq
do 
	command -v "$dep" >/dev/null || { echo "ERR: $dep not found. Please install $dep." ; exit 1 ; } 
done

process_tree() {
jq -r '
def traverse(depth):
	if .type == "workspace" then
			"WORKSPACE:" + (.name | tostring)
	elif .window then
			("  " * depth) + "* [" + (.window_properties.class // "Unknown") + "] " +
			(.name // "unnamed") + " (" + (.window | tostring) + ")" +
			(if .floating == "auto_on" then " [Floating]" else "" end)
	elif .nodes or .floating_nodes then
			("  " * depth) + "* " + (.name // ("[] (id: " + (.id | tostring) + ")"))
	else
			empty
	end,
	(.nodes // [] | map(traverse(depth + 1)) | .[] ),
	(.floating_nodes // [] | map(traverse(depth + 1)) | .[] )
;
[traverse(-1)] | .[]
' | awk '
BEGIN { print "================================================\n" }
/^WORKSPACE:/ {
	if (NR > 2) print "\n================================================\n"
	# Remove the colon from the workspace name
	sub(/^WORKSPACE:/, "")
	print "* Workspace: " $0
	next
}
/^\s*\*/ {
	sub(/^    /, "")
	print
}
END { print "\n================================================" }
'
}


if [ $# -gt 0 ] # If a file is provided as an argument, read from the file
	then process_tree < "$1"
	elif [ ! -t 0 ] # Otherwise, read from STDIN
		then process_tree
	else echo "ERR: Please provide file as argument or input from STDIN!" ; exit 1
fi
