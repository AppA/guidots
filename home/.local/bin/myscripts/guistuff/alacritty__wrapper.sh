#!/usr/bin/env sh

# Reason for this wrapper to exist:
# https://github.com/NixOS/nixpkgs/issues/122671
# https://nixos.wiki/wiki/Nixpkgs_with_OpenGL_on_non-NixOS

prefix=
[ "$(command -v nixGL)" ] && prefix="nixGL"

$prefix alacritty "$@"
