#!/usr/bin/env sh

# Set brightness level (on Intel GPUs)
# Does not require root privs IF the $bright_file is chowned by normal user.

sysdir=/sys/class/backlight/intel_backlight
bright_file="$sysdir"/brightness
read brightness_current_val < "$bright_file"  # This might look a bit superfluous at first, but look... not a cat in sight :)
read max_brightness < "$sysdir"/max_brightness

if [ ! -O "$bright_file" ] ; then $privprefix chown $USER: "$bright_file" ; fi  # Check if the file is owned by normal user and if not, change permissions.

if [ ! -n "$1" ]
	#then  # When script is run without arguments it simply prompts for the value. Not sure if I'll ever need this, so commenting it out for now.
	#	while true ; do
	#		read -r brightness_new_val
	#		if [ "$brightness_new_val" -gt 300 ] && [ "$brightness_new_val" -lt "$max_brightness" ] 2>/dev/null
	#			then echo "$brightness_new_val" > "$bright_file"
	#			else echo "Please input a value between 300 and $max_brightness" >&2
	#		fi
	#	done
	then echo "Current brightness value: $brightness_current_val"

	elif [ "$1" -eq "$1" ] 2>/dev/null  # Check whether argument is an integer (!!!)
		then
			case "$1" in  
				# If there's an argument which has a plus or minus sign in it, then
				# let the sign in the argument become the operator and let the number (integer) become an operand.
				[+]* ) brightness_new_val="$(( $brightness_current_val $1 ))"
						echo "$brightness_new_val" > "$bright_file" ;;
				[-]* ) brightness_new_val="$(( $brightness_current_val $1 ))"
						echo "$brightness_new_val" > "$bright_file" ;;
				* ) echo "$1" > "$bright_file"
			esac
		else echo "Please specify a value: [+|-]<integer>" >&2; 
fi
