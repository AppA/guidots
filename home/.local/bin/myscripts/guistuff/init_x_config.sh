#!/bin/sh

# This is a script which initializes most of my settings related to X.
# The reason that this is a script rather than a config file, is that some distros / OSes may not always support those config files.
# This script can then be sourced by other apps (like i3wm etc.)

# Set keyboard mapping to US layout, as it should be.
setxkbmap us

# Make keyboard rate non-retarded
xset r rate 280 30

# If I'm on the X220(T) keyboard, remap the XF86Back and XF86Forward keys (keys above the left and right arrows) to Prior and Next (PgDn and PgUp)
if xinput | grep -q "ThinkPad Extra Buttons"; then xmodmap -e "keycode 166=Prior" && xmodmap -e "keycode 167=Next"; fi

# Fix scrolling (this is most likely hardware dependent, and is currently only tested on X220T). 
# See xinput to get the appropriate ID if it isn't properly assigned to $touchpad below.
# To see properties of the appropriate $touchpad, then do "xinput list-props $touchpad"
# More info: https://askubuntu.com/a/830942
# TODO: Make this less hardware dependent, because "SynPS/2 Synaptics TouchPad" is very ThinkPad right now...
if xinput --list | grep -q 'SynPS/2 Synaptics TouchPad' ;
	then touchpad='SynPS/2 Synaptics TouchPad'
	else echo "Touchpad not found in xinput --list" ; exit 1
fi
#xinput_id="$(xinput --list | grep 'SynPS/2 Synaptics TouchPad' | awk '{print $6}'| cut -d'=' -f2)"

# Make (2-finger) scrolling less sensitive
xinput --set-prop "$touchpad" 'Synaptics Scrolling Distance' 303 303

# Enable 2-finger (horizontal) scrolling. https://faq.i3wm.org/question/1052/trackpad-horizontal-scrolling.1.html
xinput --set-prop "$touchpad" "Synaptics Two-Finger Scrolling" 1 1

# But disable Edge Scrolling (1-finger scroll at the edge of the touchpad)
xinput --set-prop "$touchpad" 'Synaptics Edge Scrolling' 0 0 0

# TODO: Mouse and touchpad sensitivity
# Disable mouse acceleration. See https://askubuntu.com/a/715107
xset m 0 0
# Set appropriate touchpad acceleration and deceleration
# More info: 
# https://wiki.archlinux.org/index.php/Mouse_acceleration
# https://www.x.org/wiki/Development/Documentation/PointerAcceleration/
xinput --set-prop "$touchpad" 'Device Accel Profile' 1
xinput --set-prop "$touchpad" 'Device Accel Constant Deceleration' 1.5
xinput --set-prop "$touchpad" 'Device Accel Adaptive Deceleration' 50000

# Enable tap-to-click. See: https://askubuntu.com/a/1057274
xinput --set-prop "$touchpad" 'libinput Tapping Enabled' 1
