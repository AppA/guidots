#!/bin/sh

logdir=~/logs/mpv_opened_media_links

# For some reason putting this stuff over multiple lines don't work.

#while true; do read link &&
#	if [ -n "$link" ] 
#		then
#			[ -d "$logdir" ] || mkdir -p "$logdir"
#			logfile="$logdir"/"$(date +%F_%a)"
#			echo "TS: $(date +%F_%T%z_%a)" && echo &&
#			mpv --pause --term-status-msg="" --term-playing-msg=\
#				'Path/URL: ${path} \n\
#				->>> ${media-title} \n\
#				Duration: ${duration} \n\
#				Timestamp: '"$(date +%F_%T%z_%a)" \
#				"$link" 2>&1 | \
#			sed 's/Exiting... (Quit)//' >> "$logfile" & 
#		else echo "Please provide a valid link!"
#	fi
#done


while true; do read link &&
	if [ -n "$link" ] 
		then
			[ -d "$logdir" ] || mkdir -p "$logdir"
			logfile="$logdir"/"$(date +%F_%a)"
			echo "TS: $(date +%F_%T%z_%a)" && echo &&
			# TODO: Place next line over multiple lines.
			mpv --pause --term-status-msg="" --term-playing-msg='Path/URL: ${path} \n->>> ${media-title} \nDuration: ${duration} \nTimestamp: '"$(date +%F_%T%z_%a)" "$link" 2>&1 | sed 's/Exiting... (Quit)//' >> "$logfile" & 
		else echo "Please provide a valid link!"
	fi
done
