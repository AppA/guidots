#!/bin/sh

tmux_session_name=media
tmux_window_name=aww
tmux_pane_number=1

[ -n "$1" ] &&
tmux send-keys -t "$tmux_session_name":"$tmux_window_name"."$tmux_pane_number" "$1" Space
