#!/bin/sh

nohup rofi -modi 'aww-VID:~/.local/bin/myscripts/guistuff/medialink_parsing/pass_awwlink_vid_to_tmux.sh,aww-PIC:~/.local/bin/myscripts/guistuff/medialink_parsing/pass_awwlink_pic_to_tmux.sh' -show aww-VID &
