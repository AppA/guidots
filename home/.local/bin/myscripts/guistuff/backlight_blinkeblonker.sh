#!/usr/bin/env sh

# The backlicht / brightness blinkeblonker. Blinkeblonks your backlight / brightness.
# Requires: backlightctl.sh

# Usage: backlight_blinkeblonker.sh [-r|--repeats <#_of_repeats>] [-s|--sleep-duration <number>] [-d|--divisor <number>]
# By default (smoothly) blinks 3 times.


# Arg logic
while [ $# -gt 0 ] ; do
	case $1 in
		-r|--repeats)
			if [ "$2" -eq "$2" ]
				then repeats="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else printf "ERR: Must specify the number of repeats!" ; exit 1
			fi ;;
		-s|--sleep-duration)
			if [ -n "$2" ]
				then sleep_dur="$2"
				shift 2
				else printf "ERR: Must specify the duration of the sleep / pause in between loops!" ; exit 1
			fi ;;
		-d|--divisor)
			if [ -n "$2" ]
				then divisor="$2"
				shift 2
				else printf "ERR: Must specify the divisor!" ; exit 1
			fi ;;
		*) break ;;
	esac
done

# Default values - https://wiki.bash-hackers.org/syntax/pe#assign_a_default_value
sleep_dur="${sleep_dur:=0.032}" 
divisor="${divisor:=1.23}" 
repeats="${repeats:=3}" 

# This is a wrapper, so... source the OG script.
. ~/.local/bin/myscripts/guistuff/backlightctl.sh

# Loop logic
counter=0
while [ "$counter" -lt "$repeats" ]
do
	val="$max_brightness"
	while [ "$val" -ge "300" ]
	do
		backlightctl.sh "$val"
		sleep "$sleep_dur"
		val="$(awk -v v=$val -v d=$divisor 'BEGIN { printf "%d", v / d }')"
	done
	counter=$(( counter + 1 ))
done

backlightctl.sh "$brightness_current_val"
