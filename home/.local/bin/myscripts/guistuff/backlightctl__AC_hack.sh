#!/usr/bin/env sh

# Issue: When (dis)connecting the AC of new X220T, brightness resets to the highest value.
# This is a hack around that.
# Usage: Run this script as a daemon (in a tmux session or otherwise)

brightness_file=/sys/class/backlight/intel_backlight/brightness
sysfile=/sys/class/power_supply/AC/online
# ...or /sys/class/power_supply/BAT0/status


while true
do 
	read brightness_current_val < "$brightness_file"
	read prev_val < "$sysfile"
	sleep 0.5
	read curr_val < "$sysfile"  ## So this value gets "sampled" later than $prev_val.

	if [ "$prev_val" != "$curr_val" ]  ## String comparison.
		then
			# Uncomment the following line to enable this behavior, even when the original bug is gone.
			#backlightctl.sh 4600 ; sleep 0.3
			backlightctl.sh "$brightness_current_val"
	fi
done
