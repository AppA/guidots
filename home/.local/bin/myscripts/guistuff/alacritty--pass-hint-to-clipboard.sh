#!/usr/bin/env sh

# This script passes hints (mostly URLs) from Alacritty to xclip
# Additionally it plays back a sound. 

echo -n "$1" | xclip -selection clipboard ; aplay --quiet ~/.local/share/FX-click-copy.wav
